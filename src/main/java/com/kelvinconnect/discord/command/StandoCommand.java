package com.kelvinconnect.discord.command;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;

import java.util.Random;

/**
 * Created by Adam on 21/04/2017.
 */
public class StandoCommand implements CommandExecutor {

    private final static String[] choices = { "That's what she said!", "I have ate four of Mr Kiplings cakes. He does make exceedingly good cakes.",
            "The world is flat", "Global warming is made up by the USA government", "I sleep with a tin foil hat on", "The moon landings WERE faked" , "I threw the bicycle in the Clyde",
            "I threw half away" ,"There is enough DNA in an average person’s body to stretch from the sun to Pluto and back — 17 times",
            "The average human body carries ten times more bacterial cells than human cells",
            "It can take a photon 40,000 years to travel from the core of the sun to its surface, but only 8 minutes to travel the rest of the way to Earth",
            "There are 8 times as many atoms in a teaspoonful of water as there are teaspoonfuls of water in the Atlantic ocean"
    };

    @Command(aliases = "!Stando", description = "Ask for some help from Stando.", usage = "!Stando")
    public String onStandoCommand(String[] args, DiscordAPI api) {
        String message = choices[new Random().nextInt(choices.length)];
        return "**Steven Standaloft** " +  message;
    }
}